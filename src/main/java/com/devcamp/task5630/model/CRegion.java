package com.devcamp.task5630.model;

public class CRegion {
    String regionName;
    int regionCode;

    public CRegion() {
        System.out.println("Region Khong co tham so");
    }

    public CRegion(String regionName, int regionCode) {
        this.regionName = regionName;
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public int getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(int regionCode) {
        this.regionCode = regionCode;
    }

    public CRegion instance() {
        return new CRegion();
    }
}
