package com.devcamp.task5630.model;

import java.util.List;

public class CCountry {
    String countryName;
    int countryCode;
    List<CRegion> regions;

    public CCountry(String countryName, int countryCode,  List<CRegion> regions){
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.regions = regions;
    }

    public CCountry() {
        System.out.println("Khong co tham so");
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryName = countryName;
    }

    public List<CRegion> getRegios() {
        return regions;
    }

    public void setRegions(List<CRegion> regions) {
        this.regions = regions;
    }


}
